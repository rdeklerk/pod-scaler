
import time


def get_time():
    return time.localtime()


def get_time_formatted():
    return time.strftime("%H:%M", get_time())


def get_final_minute_digit():
    return int(get_time_formatted()[-1])



