
import timer
import os
import sys
import getopt
import logging
import deployments as dep


def main() :
    try :
        opts , args = getopt.getopt(sys.argv[1:] , 'f:n:' , ['filter=' , 'namespace='])
        deployment_filter = ''
        namespace = ''

        for opt, arg in opts :
            if opt in ('-f' , '--filter'):
                deployment_filter = arg
            elif opt in ('-n' , '--namespace'):
                namespace = arg

        if deployment_filter is '' :
            deployment_filter = 'nginx'
        if namespace is '' :
            try :
                namespace = os.getenv('K8S_NAMESPACE')
            except Exception :
                namespace = 'default'

    except getopt.GetoptError as err :
        print(err)

    dep.load_config(os.getenv('KUBERNETES_SERVICE_HOST'))
    deployments = dep.Deployments(namespace)

    last_final_minute_digit = deployments.get_replicas_by_filter(deployment_filter)

    logging.info("Starting main loop...")
    while True:
        current_final_minute_digit = timer.get_final_minute_digit()

        if current_final_minute_digit != last_final_minute_digit:
            logging.info("Attempting to scale deployment with filter %s" , deployment_filter)
            deployments.scale_deployment(deployment_filter, current_final_minute_digit)


if __name__ == "__main__" :
    main()

