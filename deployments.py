
from kubernetes import client, config

import logging


def load_config(is_service_host):
    if is_service_host :
        config.load_incluster_config()
    else :
        config.load_kube_config()


def get_replicas_by_deployment(deployment):
    return deployment.spec.replicas


class Deployments :

    def __init__(self , namespace):
        self.namespace = namespace
        self.client = client.api.AppsV1Api()

    def list_deployments(self):
        return self.client.list_namespaced_deployment(self.namespace)

    def scale_deployment(self , str_filter , replicas):
        for deployment in self.list_deployments().items :
            if str_filter in deployment.metadata.name :
                if replicas != get_replicas_by_deployment(deployment) :
                    v1_scale = client.V1Scale(deployment.api_version ,
                                              deployment.kind ,
                                              deployment.metadata ,
                                              client.V1ScaleSpec(replicas=replicas))
                    self.client.replace_namespaced_deployment_scale(deployment.metadata.name ,
                                                                    self.namespace ,
                                                                    v1_scale)

                    logging.info("Deployment %s has been scaled to %s replicas" , deployment.metadata.name , replicas)

    def get_replicas_by_filter(self , str_filter):
        for deployment in self.list_deployments().items :
            if str_filter in deployment.metadata.name :
                return deployment.spec.replicas
            else :
                return 0
