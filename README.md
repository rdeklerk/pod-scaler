# Python Time-Based Pod Scaler

A python script based on the [Kubernetes client API](https://github.com/kubernetes-client/python) that scales the number
of pods in a deployment based on the current minute (e.g. 11:40 - 0 pods, 11:43 - 3 pods).

## Docker
The script's image is based on the official `python:3` image and can be pulled from the Docker hub. The current version 
is `1.1`:
    
    docker pull rdekler/time-scaler:1.2
    
## Use guide
The script uses a given `namespace` and `deployment_filter` to search for the deployment that will be scaled. The defaults
are `namespace='default'` (unless the environment variable `K8S_NAMESPACE` is set, in which case it will use that given value) 
and `deployment_filter='nginx'`, meaning that it will search the `default` namespace for a deployment containing the 
filter `nginx` and scale the first one that it finds.

Both of these values can be modified at runtime by running the script with the following args:

- `-f 'deployment_filter'` or `--filter 'deployment_filter` will change the deployment filter string
- `-n 'namespace'` or `--namespace 'namespace` will change the namespace the script will search
 
 It is important to ensure that the script has proper authorization to operate in any namespace, including the one to which
 it has been deployed. The included Helm chart creates a `ServiceAccount` with the required permissions in the namespace
 to which it is installed. It also includes the namespace as an environment variable for the script to read. As such, the
 script must be installed in the same namespace as the deployment it is searching for.